package horadorancho.krahl.com.br.horadorancho.itens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;
import horadorancho.krahl.com.br.horadorancho.departamento.Departamento;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.lista.ListaListasActivity;
import horadorancho.krahl.com.br.horadorancho.listasAbertasActivity;
import horadorancho.krahl.com.br.horadorancho.produto.Produto;
import horadorancho.krahl.com.br.horadorancho.tipouni.TipoUni;

public class ItensActivity extends Activity implements ListView.OnItemClickListener{
    private int idLista;
    private List<Item> itemLista = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Item, Integer> getDao() throws SQLException {
        Dao<Item, Integer> dao = DaoManager.createDao(getConnectionSource(), Item.class);
        return dao;
    }

    private Dao<Departamento, Integer> getDepartamentoDao() throws SQLException {
        Dao<Departamento, Integer> dao = DaoManager.createDao(getConnectionSource(), Departamento.class);
        return dao;
    }

    private Dao<Produto, Integer> getProdutoDao() throws SQLException {
        Dao<Produto, Integer> dao = DaoManager.createDao(getConnectionSource(), Produto.class);
        return dao;
    }

    private Dao<TipoUni, Integer> getTipoUniDao() throws SQLException {
        Dao<TipoUni, Integer> dao = DaoManager.createDao(getConnectionSource(), TipoUni.class);
        return dao;
    }

    private Dao<Lista, Integer> getListaDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itens);

        ListView lista = (ListView) findViewById(R.id.listaProdutosLista);

        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            idLista = parametros.getInt("id");
            Lista pegaTitulo = null;

            try {
                pegaTitulo = getListaDao().queryForId(idLista);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            setTitle("Lista " + pegaTitulo.getId() + " - " + pegaTitulo.getNome());

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, preencheDados());
            lista.setAdapter(arrayAdapter);
            lista.setOnItemClickListener(this);

        }
    }

    public ArrayList<String> preencheDados() {
        ArrayList<String> dados = new ArrayList<String>();
        try {
            itemLista = getDao().queryForEq("lista_id", idLista);
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        String retorno = "";
        for(int i = 0; i < itemLista.size(); i++){
            retorno = "";

            retorno = getDepartamentoNome(Integer.parseInt(String.valueOf(itemLista.get(i).getDepartamento_id())));
            retorno += " - ";
            retorno += getProdutoNome(Integer.parseInt(String.valueOf(itemLista.get(i).getProduto_id())));
            retorno += " - ";
            retorno += String.valueOf(itemLista.get(i).getQtd());
            retorno += " ";
            retorno += getTipoUniNome(Integer.parseInt(String.valueOf(itemLista.get(i).getTipouni_id())));

            dados.add(retorno);
        }

        return dados;

    }

    public String getDepartamentoNome(int id){
        Departamento dep = null;
        try {
            dep  = getDepartamentoDao().queryForId(id);
            return dep.getNome();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public String getProdutoNome(int id){
        Produto prod = null;
        try {
            prod  = getProdutoDao().queryForId(id);
            return prod.getNome();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public String getTipoUniNome(int id){
        TipoUni tipoUni = null;
        try {
            tipoUni  = getTipoUniDao().queryForId(id);
            return tipoUni.getSigla();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public void adicionarProduto(View view) {
        Intent intent = new Intent(ItensActivity.this, produtosListaActivity.class);
        intent.putExtra("id",idLista);
        intent.putExtra("idItem",0);
        startActivity(intent);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String teste = parent.getItemAtPosition(position);
        Intent intent = new Intent(ItensActivity.this, produtosListaActivity.class);

        intent.putExtra("id",idLista);
        intent.putExtra("idItem", itemLista.get(position).getId().intValue());
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(this, ListaListasActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        super.onDestroy();
    }

}
