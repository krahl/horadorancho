package horadorancho.krahl.com.br.horadorancho.itens;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "item")
public class Item {
    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private double qtd;

    @DatabaseField
    private String flagcomprado;

    @DatabaseField
    private Long departamento_id;

    @DatabaseField
    private Long tipouni_id;

    @DatabaseField
    private Long produto_id;

    @DatabaseField
    private Long lista_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getQtd() {
        return qtd;
    }

    public void setQtd(double qtd) {
        this.qtd = qtd;
    }

    public String getFlagcomprado() {
        return flagcomprado;
    }

    public void setFlagcomprado(String flagcomprado) {
        this.flagcomprado = flagcomprado;
    }

    public Long getDepartamento_id() {
        return departamento_id;
    }

    public void setDepartamento_id(Long departamento_id) {
        this.departamento_id = departamento_id;
    }

    public Long getTipouni_id() {
        return tipouni_id;
    }

    public void setTipouni_id(Long tipouni_id) {
        this.tipouni_id = tipouni_id;
    }

    public Long getProduto_id() {
        return produto_id;
    }

    public void setProduto_id(Long produto_id) {
        this.produto_id = produto_id;
    }

    public Long getLista_id() {
        return lista_id;
    }

    public void setLista_id(Long lista_id) {
        this.lista_id = lista_id;
    }

    public Item() {
    }


    public Item(double qtd, Long departamento_id, Long tipouni_id, Long produto_id, Long lista_id) {
        this.qtd = qtd;
        this.departamento_id = departamento_id;
        this.tipouni_id = tipouni_id;
        this.produto_id = produto_id;
        this.lista_id = lista_id;
    }

    @Override
    public String toString() {
        return "Item [id = "+id+", qtd = " + qtd +", depart = "+departamento_id+", produto = "+produto_id+ "]";
    }

    public boolean comprado(){
        if(flagcomprado != null){
            return flagcomprado.toUpperCase().equals("T");
        }
        return false;
    }
}
