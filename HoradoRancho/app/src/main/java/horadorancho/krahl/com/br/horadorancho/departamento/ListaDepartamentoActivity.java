package horadorancho.krahl.com.br.horadorancho.departamento;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.melnykov.fab.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;

public class ListaDepartamentoActivity extends Activity implements ListView.OnItemClickListener{

    private List<Departamento> deparamentoLista = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Departamento, Integer> getDepartamentoDao() throws SQLException {
        Dao<Departamento, Integer> dao = DaoManager.createDao(getConnectionSource(), Departamento.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_departamento);

        ListView lista = (ListView) findViewById(R.id.listaDep);
        ArrayList<String> deps = preencheDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, deps);
        lista.setAdapter(arrayAdapter);

        lista.setOnItemClickListener(this);
    }

    private ArrayList<String> preencheDados() {
        ArrayList<String> dados = new ArrayList<String>();

        try {
            deparamentoLista = getDepartamentoDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        for(int i = 0; i < deparamentoLista.size(); i++){
            dados.add(deparamentoLista.get(i).getNome().toString());
        }

        return dados;

    }

    public void adicionarDepartamento(View view) {
        Intent intent = new Intent(ListaDepartamentoActivity.this, DepartamentoActivity.class);
        startActivity(intent);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String teste = parent.getItemAtPosition(position);
        Intent intent = new Intent(ListaDepartamentoActivity.this, DepartamentoActivity.class);

        intent.putExtra("id",deparamentoLista.get(position).getId().intValue());
        startActivity(intent);
    }
}
