package horadorancho.krahl.com.br.horadorancho;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.departamento.Departamento;
import horadorancho.krahl.com.br.horadorancho.itens.Item;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.produto.Produto;
import horadorancho.krahl.com.br.horadorancho.tipouni.TipoUni;

public class comprar extends Activity {
    long idLista =0;

    MyCustomAdapter dataAdapter = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Item, Integer> getDao() throws SQLException {
        Dao<Item, Integer> dao = DaoManager.createDao(getConnectionSource(), Item.class);
        return dao;
    }

    private Dao<Departamento, Integer> getDepartamentoDao() throws SQLException {
        Dao<Departamento, Integer> dao = DaoManager.createDao(getConnectionSource(), Departamento.class);
        return dao;
    }

    private Dao<Lista, Integer> getListaDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    private Dao<Produto, Integer> getProdutoDao() throws SQLException {
        Dao<Produto, Integer> dao = DaoManager.createDao(getConnectionSource(), Produto.class);
        return dao;
    }

    private Dao<TipoUni, Integer> getTipoUniDao() throws SQLException {
        Dao<TipoUni, Integer> dao = DaoManager.createDao(getConnectionSource(), TipoUni.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprar);
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            idLista = parametros.getLong("id");
            displayListView();
        }

    }

    private void displayListView() {

        ArrayList<Item> listaItens = new ArrayList<Item>();
        List<Item> itens = null;
        try {
            itens = getDao().queryForEq("lista_id", idLista);
            listaItens.addAll(itens);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dataAdapter = new MyCustomAdapter(this,R.layout.marcarcomprado, listaItens);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);
    }

    private class MyCustomAdapter extends ArrayAdapter<Item> {

        private ArrayList<Item> itens;

        public MyCustomAdapter(Context context, int textViewResourceId,ArrayList<Item> itemLista) {
            super(context, textViewResourceId, itemLista);
            this.itens = new ArrayList<Item>();
            this.itens.addAll(itemLista);
        }

        private class ViewHolder {
            TextView descricao;
            TextView subtexto;
            TextView departamento;
            CheckBox comprado;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.marcarcomprado, null);

                holder = new ViewHolder();
                holder.descricao = (TextView) convertView.findViewById(R.id.texto);
                holder.subtexto = (TextView) convertView.findViewById(R.id.subtexto);
                holder.departamento = (TextView) convertView.findViewById(R.id.departamento);
                holder.comprado = (CheckBox) convertView.findViewById(R.id.checkComprado);
                convertView.setTag(holder);

                holder.comprado.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;

                        Item item = itens.get(position);
                        if (cb.isChecked()){
                            item.setFlagcomprado("T");
                        }else{
                            item.setFlagcomprado("F");
                        }

                        try {
                            if (getDao().update(item) < 1){
                                Toast.makeText(getApplicationContext(), "Falha ao gravar", Toast.LENGTH_LONG).show();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            //Adiciona os Itens na Lista
            Item item = itens.get(position);
            holder.descricao.setText(getProdutoNome(Integer.parseInt(String.valueOf(item.getProduto_id()))));
            holder.subtexto.setText("Quantidade: " + item.getQtd()+" "+getTipoUniNome(Integer.parseInt(String.valueOf(item.getTipouni_id()))));
            holder.departamento.setText("Dep. "+getDepartamentoNome(Integer.parseInt(String.valueOf(item.getDepartamento_id()))));
            holder.comprado.setText("");
            holder.comprado.setChecked(item.comprado());

            return convertView;
        }

    }

    public String getDepartamentoNome(int id){
        Departamento dep = null;
        try {
            dep  = getDepartamentoDao().queryForId(id);
            return dep.getNome();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public String getProdutoNome(int id){
        Produto prod = null;
        try {
            prod  = getProdutoDao().queryForId(id);
            return prod.getNome();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public String getTipoUniNome(int id){
        TipoUni tipoUni = null;
        try {
            tipoUni  = getTipoUniDao().queryForId(id);
            return tipoUni.getSigla();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(this, listasAbertasActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        super.onDestroy();
    }
}