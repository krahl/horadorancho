package horadorancho.krahl.com.br.horadorancho.tipouni;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;

public class TipoUniActivity extends Activity {
    private boolean alteracao = false;
    private int idDB = 0;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<TipoUni, Integer> getDao() throws SQLException {
        Dao<TipoUni, Integer> dao = DaoManager.createDao(getConnectionSource(), TipoUni.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_uni);

        //Caso for alterar pega o ID por parametro
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            EditText mEditNome, mEditSigla;
            idDB = parametros.getInt("id");

            mEditNome   = (EditText)findViewById(R.id.editTipo);
            mEditSigla    = (EditText)findViewById(R.id.editSigla);

            TipoUni tipoUni = null;
            try {
                tipoUni = getDao().queryForId(idDB);
            } catch (SQLException e) {
                Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            mEditNome.setText(tipoUni.getNome().toString());
            mEditSigla.setText(tipoUni.getSigla().toString());
            alteracao = true;

        }else{
            //Quando é um registro novo nao se deve excluir
            Button bExcluir = (Button)findViewById(R.id.buttonExcluir);
            bExcluir.setVisibility(View.INVISIBLE);
        }

    }

    public void salvar(View view) throws SQLException {
        EditText mEditNome, mEditSigla;
        mEditNome   = (EditText)findViewById(R.id.editTipo);
        mEditSigla    = (EditText)findViewById(R.id.editSigla);

        if (validar()){
            if(alteracao){
                TipoUni tipoUni = null;

                try {
                    tipoUni = getDao().queryForId(idDB);
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                tipoUni.setNome(mEditNome.getText().toString());
                tipoUni.setSigla(mEditSigla.getText().toString());

                try {
                    if (getDao().update(tipoUni) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }else{
                TipoUni tipoUni = new TipoUni(mEditNome.getText().toString(),mEditSigla.getText().toString());

                try {
                    if(getDao().create(tipoUni) < 1)
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    public void cancelar(View view) {
        Intent intent = new Intent(this, ListaTipoUniActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void excluir(View view){
        try {
            TipoUni tipoUni = getDao().queryForId(idDB);
            if(getDao().delete(tipoUni) < 1){
                Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
            }else{
                cancelar(view);
            }
        } catch (SQLException e) {
            Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public boolean validar(){
        EditText mEditNome, mEditSigla;
        mEditNome   = (EditText)findViewById(R.id.editTipo);
        mEditSigla    = (EditText)findViewById(R.id.editSigla);

        if (mEditNome.getText().toString().length() < 1 || mEditSigla.getText().toString().length() < 1){
            Toast.makeText(this, "Informe os campos corretamente!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
