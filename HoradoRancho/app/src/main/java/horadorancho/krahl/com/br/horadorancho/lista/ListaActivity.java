package horadorancho.krahl.com.br.horadorancho.lista;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.itens.Item;
import horadorancho.krahl.com.br.horadorancho.itens.ItensActivity;
import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;
import horadorancho.krahl.com.br.horadorancho.modelo.modeloActivity;
import horadorancho.krahl.com.br.horadorancho.tag.Tag;

public class ListaActivity extends Activity {
    private boolean alteracao = false;
    private int idDB = 0;
    private List<Tag> tagLista = null;
    ArrayList<Long> idTags;
    private boolean modelo = false;

    private ConnectionSource getConnectionSource() {
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Tag, Integer> getTagDao() throws SQLException {
        Dao<Tag, Integer> dao = DaoManager.createDao(getConnectionSource(), Tag.class);
        return dao;
    }

    private Dao<Lista, Integer> getDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    private Dao<Item, Integer> getItemDao() throws SQLException {
        Dao<Item, Integer> dao = DaoManager.createDao(getConnectionSource(), Item.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Spinner dropdown = (Spinner)findViewById(R.id.dropdownTag);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, carregaTags());
        dropdown.setAdapter(adapter);

        //Caso for alterar pega o ID por parametro
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            EditText mEditNome;
            CheckBox cBox;
            //Spinner dropTag;

            mEditNome   = (EditText)findViewById(R.id.editListaNome);
            cBox        = (CheckBox)findViewById(R.id.checkBoxModelo);

            if( parametros.getInt("modelo") != 1){
                idDB = parametros.getInt("id");

                Lista lista = null;
                try {
                    lista = getDao().queryForId(idDB);
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                //Carrega os dados
                mEditNome.setText(lista.getNome().toString());
                cBox.setChecked(lista.getFlagmodelo().toString().equals("T"));
                dropdown.setSelection(idTags.indexOf(lista.getTag_id()));

                if ( parametros.getInt("modelo") == 2){
                    cBox.setEnabled(false);
                    modelo = true;
                }
                alteracao = true;
            }else{
                cBox.setChecked(true);
                cBox.setEnabled(false);
                setTitle("Novo Modelo");
                modelo = true;
            }

        }else{
            //Quando é um registro novo nao se deve excluir
            Button bExcluir = (Button)findViewById(R.id.buttonExcluir);
            bExcluir.setVisibility(View.INVISIBLE);
            //Somente apos gravar pode incluir produtos
            Button bProdutos = (Button)findViewById(R.id.buttonNovoProduto);
            bProdutos.setVisibility(View.INVISIBLE);
        }
    }

    private ArrayList<String> carregaTags() {
        ArrayList<String> dados = new ArrayList<String>();
        ArrayList<Long> ids = new ArrayList<Long>();
        try {
            tagLista = getTagDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        for(int i = 0; i < tagLista.size(); i++){
            dados.add(tagLista.get(i).getId() +" - "+ tagLista.get(i).getNome().toString());
            ids.add(tagLista.get(i).getId());
        }

        idTags = ids;
        return dados;

    }
    //Falta o validar
    public boolean validar(){
        EditText mEditNome;
        mEditNome   = (EditText)findViewById(R.id.editListaNome);

        if (mEditNome.getText().toString().length() < 1){
            Toast.makeText(this, "Informe os campos corretamente!", Toast.LENGTH_LONG).show();
            return false;
        }
        Spinner dropdown = (Spinner)findViewById(R.id.dropdownTag);

        if(dropdown.getSelectedItemPosition() == AdapterView.INVALID_POSITION){
            Toast.makeText(this, "Favor selecione uma Tag!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void salvar(View view) throws SQLException {
        EditText mEditNome;
        CheckBox cBox;
        Spinner dropTag;

        mEditNome   = (EditText)findViewById(R.id.editListaNome);
        cBox        = (CheckBox)findViewById(R.id.checkBoxModelo);
        dropTag     = (Spinner)findViewById(R.id.dropdownTag);

        if (validar()){
            if(alteracao){
                Lista lista = null;

                try {
                    lista = getDao().queryForId(idDB);
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                String modelo = "";
                if(cBox.isChecked()){
                    modelo="T";
                }else{
                    modelo="F";
                };

                lista.setNome(mEditNome.getText().toString());
                lista.setFlagmodelo(modelo);
                //tags
                long idtag = idTags.get(dropTag.getSelectedItemPosition());
                lista.setTag_id(idtag);

                try {
                    if (getDao().update(lista) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else {
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }else{

                String modelo = "";
                if(cBox.isChecked()){
                    modelo="T";
                }else{
                    modelo="F";
                };
                long idtag = idTags.get(dropTag.getSelectedItemPosition());
                Lista lista = new Lista(mEditNome.getText().toString(), null, modelo, idtag);

                try {
                    if(getDao().create(lista) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else {
                        idDB = Integer.parseInt(String.valueOf(lista.getId()));
                        pAddProdLista(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    public void pAddProdLista(final View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Deseja Adicionar Itens a Lista?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){

                        addproduto(view);
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        cancelar(view);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void cancelar(View view) {
        Intent intent = null;
        if (modelo){
            intent = new Intent(this, modeloActivity.class);
        }else{
            intent = new Intent(this, ListaListasActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void addproduto(View view) {
        Intent intent = new Intent(ListaActivity.this, ItensActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", idDB);
        startActivity(intent);
    }

    public boolean excluirItens(View view){
        List<Item> itens = null;

        try {
            itens = getItemDao().queryForEq("lista_id", idDB);
            if(itens.size() > 0){
                if(getItemDao().delete(itens) > 0){
                    return true;
                }else{
                    Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                }
            }else {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void excluir(View view){
        try {
            if(excluirItens(view)){
                Lista lista = getDao().queryForId(idDB);
                if(getDao().delete(lista) < 1){
                    Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                }else{
                    cancelar(view);
                }
            }
        } catch (SQLException e) {
            Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
