package horadorancho.krahl.com.br.horadorancho.departamento;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;

public class DepartamentoActivity extends Activity {
    private boolean alteracao = false;
    private int idDepartamento = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departamento);

        //Caso for alterar pega o ID por parametro
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            EditText mEditNome, mEditCor;
            idDepartamento = parametros.getInt("id");

            mEditNome   = (EditText)findViewById(R.id.editDepartamento);
            mEditCor    = (EditText)findViewById(R.id.editCor);

            Departamento deparamento = null;
            try {
                deparamento = getDepartamentoDao().queryForId(idDepartamento);
            } catch (SQLException e) {
                Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            mEditNome.setText(deparamento.getNome().toString());
            mEditCor.setText(deparamento.getCor().toString());
            alteracao = true;

        }else{
            //Quando é um registro novo nao se deve excluir
            Button bExcluir = (Button)findViewById(R.id.buttonExcluir);
            bExcluir.setVisibility(View.INVISIBLE);
        }

    }

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(DepartamentoActivity.this).getConnectionSource();
    }

    private Dao<Departamento, Integer> getDepartamentoDao() throws SQLException {
        Dao<Departamento, Integer> dao = DaoManager.createDao(getConnectionSource(), Departamento.class);
        return dao;
    }

    public void salvaDepartamento(View view) throws SQLException {
        EditText mEditNome, mEditCor;
        mEditNome   = (EditText)findViewById(R.id.editDepartamento);
        mEditCor    = (EditText)findViewById(R.id.editCor);

        if (validar()){
            if(alteracao){
                Departamento departamento = null;

                try {
                    departamento = getDepartamentoDao().queryForId(idDepartamento);
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                departamento.setNome(mEditNome.getText().toString());
                departamento.setCor(mEditCor.getText().toString());

                try {
                    if (getDepartamentoDao().update(departamento) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }else{
                Departamento dep = new Departamento(mEditNome.getText().toString(),mEditCor.getText().toString());

                try {
                    if(getDepartamentoDao().create(dep) < 1)
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    public void cancelar(View view) {
        Intent intent = new Intent(DepartamentoActivity.this, ListaDepartamentoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void excluir(View view){
        try {
            Departamento departamento = getDepartamentoDao().queryForId(idDepartamento);
            if(getDepartamentoDao().delete(departamento) < 1){
                Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
            }else{
                cancelar(view);
            }
        } catch (SQLException e) {
            Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public boolean validar(){
        EditText mEditNome, mEditCor;
        mEditNome   = (EditText)findViewById(R.id.editDepartamento);
        mEditCor    = (EditText)findViewById(R.id.editCor);

        if (mEditNome.getText().toString().length() < 1 || mEditCor.getText().toString().length() < 1){
            Toast.makeText(this, "Informe os campos corretamente!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
