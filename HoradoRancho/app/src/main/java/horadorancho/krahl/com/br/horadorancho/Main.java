package horadorancho.krahl.com.br.horadorancho;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

import horadorancho.krahl.com.br.horadorancho.departamento.ListaDepartamentoActivity;
import horadorancho.krahl.com.br.horadorancho.lista.ListaListasActivity;
import horadorancho.krahl.com.br.horadorancho.modelo.modeloActivity;
import horadorancho.krahl.com.br.horadorancho.produto.ListaProdutoActivity;
import horadorancho.krahl.com.br.horadorancho.tag.ListaTagActivity;
import horadorancho.krahl.com.br.horadorancho.tipouni.ListaTipoUniActivity;

public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Conexao com o Banco
        try {
            testOutOrmLiteDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testOutOrmLiteDatabase() throws SQLException {
        OpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                OpenDatabaseHelper.class);
    }

    public void novoDepartamento(View view) {
        Intent intent = new Intent(this, ListaDepartamentoActivity.class);
        startActivity(intent);
    }

    public void tipoUni(View view) {
        Intent intent = new Intent(this, ListaTipoUniActivity.class);
        startActivity(intent);
    }

    public void tag(View view) {
        Intent intent = new Intent(this, ListaTagActivity.class);
        startActivity(intent);
    }

    public void produto(View view) {
        Intent intent = new Intent(this, ListaProdutoActivity.class);
        startActivity(intent);
    }

    public void lista(View view) {
        Intent intent = new Intent(this, ListaListasActivity.class);
        startActivity(intent);
    }

    public void modelos(View view) {
        Intent intent = new Intent(this, modeloActivity.class);
        startActivity(intent);
    }
    public void comprarr(View view) {
        Intent intent = new Intent(this, listasAbertasActivity.class);
        startActivity(intent);
    }

    public void sobresistema(View view) {
        Intent intent = new Intent(this, SobreSistemaActivity.class);
        startActivity(intent);
    }
}
