package horadorancho.krahl.com.br.horadorancho.tag;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tags")
public class Tag {
    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private String nome;

    public Tag() {

    }

    public Tag(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Tag [id = "+id+", nome = " + nome + "]";
    }
}
