package horadorancho.krahl.com.br.horadorancho.lista;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "listas")
public class Lista {
    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField
    private String nome;

    @DatabaseField
    private String flagmodelo;

    @DatabaseField
    private Long tag_id;

    public Lista() {
    }

    public Lista(String nome, Date dt, String flagmodelo, long tag_id) {
        this.nome = nome;
        this.flagmodelo = flagmodelo;
        this.tag_id = tag_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFlagmodelo() {
        return flagmodelo;
    }

    public void setFlagmodelo(String flagmodelo) {
        this.flagmodelo = flagmodelo;
    }

    public long getTag_id() {
        return tag_id;
    }

    public void setTag_id(long tag_id) {
        this.tag_id = tag_id;
    }

    @Override
    public String toString() {
        return "Lista [id = "+id+", nome = " + nome +", modelo = "+flagmodelo+", tag = "+tag_id+ "]";
    }
}
