package horadorancho.krahl.com.br.horadorancho;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import horadorancho.krahl.com.br.horadorancho.departamento.Departamento;
import horadorancho.krahl.com.br.horadorancho.itens.Item;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.produto.Produto;
import horadorancho.krahl.com.br.horadorancho.tag.Tag;
import horadorancho.krahl.com.br.horadorancho.tipouni.TipoUni;

public class OpenDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "horadorancho";
    //Toda vez que alterar as tabelas alterar a versão.
    private static final int DATABASE_VERSION = 6;

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<Departamento, Long> departamentoDao;
    private Dao<Item, Long> itemDao;
    private Dao<Lista, Long> listaDao;
    private Dao<Produto, Long> produtoDao;
    private Dao<Tag, Long> tagDao;
    private Dao<TipoUni, Long> tipoUniDao;

    public OpenDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static ConnectionSource getConnectionSource(Context context) {
        return new OpenDatabaseHelper(context).getConnectionSource();
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, Departamento.class);
            TableUtils.createTable(connectionSource, Item.class);
            TableUtils.createTable(connectionSource, Lista.class);
            TableUtils.createTable(connectionSource, Produto.class);
            TableUtils.createTable(connectionSource, Tag.class);
            TableUtils.createTable(connectionSource, TipoUni.class);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, Departamento.class, false);
            TableUtils.dropTable(connectionSource, Item.class, false);
            TableUtils.dropTable(connectionSource, Lista.class, false);
            TableUtils.dropTable(connectionSource, Produto.class, false);
            TableUtils.dropTable(connectionSource, Tag.class, false);
            TableUtils.dropTable(connectionSource, TipoUni.class, false);

            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<Departamento, Long> getDaoDepartamento() throws SQLException {
        if(departamentoDao == null) {
            departamentoDao = getDao(Departamento.class);
        }
        return departamentoDao;
    }

    public Dao<Item, Long> getDaoItem() throws SQLException {
        if(itemDao == null) {
            itemDao = getDao(Item.class);
        }
        return itemDao;
    }

    public Dao<Lista, Long> getDaoLista() throws SQLException {
        if(listaDao == null) {
            listaDao = getDao(Lista.class);
        }
        return listaDao;
    }

    public Dao<Produto, Long> getDaoProduto() throws SQLException {
        if(produtoDao == null) {
            produtoDao = getDao(Produto.class);
        }
        return produtoDao;
    }

    public Dao<Tag, Long> getDaoTag() throws SQLException {
        if(tagDao == null) {
            tagDao = getDao(Tag.class);
        }
        return tagDao;
    }

    public Dao<TipoUni, Long> getDaoTipoUni() throws SQLException {
        if(tipoUniDao == null) {
            tipoUniDao = getDao(TipoUni.class);
        }
        return tipoUniDao;
    }
}