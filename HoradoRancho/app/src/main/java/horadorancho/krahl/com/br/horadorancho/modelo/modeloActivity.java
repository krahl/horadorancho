package horadorancho.krahl.com.br.horadorancho.modelo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.lista.ListaActivity;

public class modeloActivity extends Activity implements ListView.OnItemClickListener{

    private List<Lista> listaLista = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Lista, Integer> getDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modelo);

        ListView lista = (ListView) findViewById(R.id.listaModelo);
        ArrayList<String> listas = preencheDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listas);
        lista.setAdapter(arrayAdapter);

        lista.setOnItemClickListener(this);
    }

    private ArrayList<String> preencheDados() {
        ArrayList<String> dados = new ArrayList<String>();

        try {
            listaLista = getDao().queryForEq("flagmodelo","T");
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        for(int i = 0; i < listaLista.size(); i++){
            dados.add(listaLista.get(i).getNome().toString());
        }

        return dados;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modelo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void adicionarLista(View view) {
        Intent intent = new Intent(this, ListaActivity.class);
        intent.putExtra("modelo",1);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String teste = parent.getItemAtPosition(position);
        Intent intent = new Intent(this, ListaActivity.class);

        intent.putExtra("id",listaLista.get(position).getId().intValue());
        intent.putExtra("modelo",2);
        startActivity(intent);
    }
}