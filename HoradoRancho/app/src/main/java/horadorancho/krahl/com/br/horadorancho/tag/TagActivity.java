package horadorancho.krahl.com.br.horadorancho.tag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;

public class TagActivity extends Activity {
    private boolean alteracao = false;
    private int idDB = 0;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Tag, Integer> getDao() throws SQLException {
        Dao<Tag, Integer> dao = DaoManager.createDao(getConnectionSource(), Tag.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);

        //Caso for alterar pega o ID por parametro
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            EditText mEditNome;
            idDB = parametros.getInt("id");

            mEditNome   = (EditText)findViewById(R.id.editTag);

            Tag tag = null;
            try {
                tag = getDao().queryForId(idDB);
            } catch (SQLException e) {
                Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            mEditNome.setText(tag.getNome().toString());
            alteracao = true;

        }else{
            //Quando é um registro novo nao se deve excluir
            Button bExcluir = (Button)findViewById(R.id.buttonExcluir);
            bExcluir.setVisibility(View.INVISIBLE);
        }
    }

    public void salvar(View view) throws SQLException {
        EditText mEditNome;
        mEditNome   = (EditText)findViewById(R.id.editTag);

        if (validar()){
            if(alteracao){
                Tag tag = null;

                try {
                    tag = getDao().queryForId(idDB);
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                tag.setNome(mEditNome.getText().toString());

                try {
                    if (getDao().update(tag) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }else{
                Tag tag = new Tag(mEditNome.getText().toString());

                try {
                    if(getDao().create(tag) < 1)
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    public void cancelar(View view) {
        Intent intent = new Intent(this, ListaTagActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void excluir(View view){
        try {
            Tag tag = getDao().queryForId(idDB);
            if(getDao().delete(tag) < 1){
                Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
            }else{
                cancelar(view);
            }
        } catch (SQLException e) {
            Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public boolean validar(){
        EditText mEditNome;
        mEditNome   = (EditText)findViewById(R.id.editTag);

        if (mEditNome.getText().toString().length() < 1){
            Toast.makeText(this, "Informe os campos corretamente!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
