package horadorancho.krahl.com.br.horadorancho;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.itens.Item;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.produto.Produto;

public class listasAbertasActivity extends Activity {
    Long idLista;
    MyCustomAdapter dataAdapter = null;
    Boolean ib_Todos = false;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Lista, Integer> getListaDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    private Dao<Item, Integer> getItemDao() throws SQLException {
        Dao<Item, Integer> dao = DaoManager.createDao(getConnectionSource(), Item.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listas_abertas);

        displayListView();
    }

    public boolean listaAberta(long id){
        Boolean ib_Retorno = false;
        List<Item> itens = null;
        try {
            itens = getItemDao().queryForEq("lista_id",id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (itens.size() > 0){
            for (int i=0; i < itens.size(); i++){
                if (!itens.get(i).comprado()){
                    ib_Retorno = true;
                }
            }
        }
        return ib_Retorno;
    }

    public void mostar(View view){
        CheckBox checkBox = (CheckBox)findViewById(R.id.checkBox);
        ib_Todos = checkBox.isChecked();
        displayListView();
    }

    private void displayListView() {

        ArrayList<Lista> listaItens = new ArrayList<Lista>();
        List<Lista> itens = null;
        try {
            itens = getListaDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(ib_Todos){
            listaItens.addAll(itens);
        }else {
            int size = itens.size();
            int[] pos = new int[itens.size()];

            for (int i=0; i < size; i++){
                if (!listaAberta(itens.get(i).getId())){
                    pos[i] = i;
                }else{
                    pos[i] = -1;
                }
            }

            for (int i=size-1; i >= 0; i--){
                if (pos[i] != -1){
                    itens.remove(i);
                }
            }
            listaItens.addAll(itens );
        }

        dataAdapter = new MyCustomAdapter(this,R.layout.listas_abertas, listaItens);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);

        final List<Lista> finalItens = itens;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idLista = finalItens.get(position).getId();
                comprar(view);
            }
        });

    }

    private class MyCustomAdapter extends ArrayAdapter<Lista> {

        private ArrayList<Lista> itens;

        public MyCustomAdapter(Context context, int textViewResourceId,ArrayList<Lista> itemLista) {
            super(context, textViewResourceId, itemLista);
            this.itens = new ArrayList<Lista>();
            this.itens.addAll(itemLista);
        }

        private class ViewHolder {
            TextView descricao;
            ProgressBar progresso;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.listas_abertas, null);

                holder = new ViewHolder();
                holder.descricao = (TextView) convertView.findViewById(R.id.texto);
                holder.progresso = (ProgressBar)convertView.findViewById(R.id.progressBar2);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            //Adiciona os Itens na Lista
            Lista item = itens.get(position);
            holder.descricao.setText(getListaNome(Integer.parseInt(String.valueOf(item.getId()))));
            //
            List<Item> itens = null;
            try {
                itens  = getItemDao().queryForEq("lista_id", Integer.parseInt(String.valueOf(item.getId())));
            } catch (SQLException e) {
                Toast.makeText(getContext(), "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            if(itens.size() > 0){
                holder.progresso.setMax(itens.size());
                int progres =0;
                for (int i =0; i < itens.size(); i++){
                    if(itens.get(i).comprado()){
                        progres = progres+1;
                    }
                }
                holder.progresso.setProgress(progres);
            }
            return convertView;
        }

    }

    public String getListaNome(int id){
        Lista lista = null;
        try {
            lista  = getListaDao().queryForId(id);
            return lista.getNome();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return "";
    }

    public void comprar(View view) {
        Intent intent = new Intent(this, comprar.class);
        intent.putExtra("id",idLista);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
