package horadorancho.krahl.com.br.horadorancho.itens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;
import horadorancho.krahl.com.br.horadorancho.departamento.Departamento;
import horadorancho.krahl.com.br.horadorancho.lista.Lista;
import horadorancho.krahl.com.br.horadorancho.produto.Produto;
import horadorancho.krahl.com.br.horadorancho.tipouni.TipoUni;

public class produtosListaActivity extends Activity {
    private int idListaInt = 0;
    private int idItem =0;
    private boolean alteracao = false;
    private ArrayList<Long> idDepartamentos, idListas, idProdutos, idTiposUni = new ArrayList<Long>();
    private List<Departamento> deparmentoLista = null;
    private List<Produto> produtoLista = null;
    private List<TipoUni> tipoUniLista = null;
    private Item item;

    private ConnectionSource getConnectionSource() {
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Departamento, Integer> getDepartamentoDao() throws SQLException {
        Dao<Departamento, Integer> dao = DaoManager.createDao(getConnectionSource(), Departamento.class);
        return dao;
    }

    private Dao<Lista, Integer> getListaDao() throws SQLException {
        Dao<Lista, Integer> dao = DaoManager.createDao(getConnectionSource(), Lista.class);
        return dao;
    }

    private Dao<Produto, Integer> getProdutoDao() throws SQLException {
        Dao<Produto, Integer> dao = DaoManager.createDao(getConnectionSource(), Produto.class);
        return dao;
    }

    private Dao<TipoUni, Integer> getTipoUniDao() throws SQLException {
        Dao<TipoUni, Integer> dao = DaoManager.createDao(getConnectionSource(), TipoUni.class);
        return dao;
    }

    private Dao<Item, Integer> getItemDao() throws SQLException {
        Dao<Item, Integer> dao = DaoManager.createDao(getConnectionSource(), Item.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos_lista);

        Spinner dropdownDepartamentos = (Spinner)findViewById(R.id.dropdownDepartamento);
        Spinner dropdownProdutos = (Spinner)findViewById(R.id.dropdownProduto);
        Spinner dropdownTipoUni = (Spinner)findViewById(R.id.dropdownTipoUni);
        EditText mEditQtd = (EditText)findViewById(R.id.editTextQtd);

        //Carregar os dados
        ArrayAdapter<String> adapterDepartamento = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, carregaDeps());
        dropdownDepartamentos.setAdapter(adapterDepartamento);

        ArrayAdapter<String> adapterProduto = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, carregaProdutos());
        dropdownProdutos.setAdapter(adapterProduto);

        ArrayAdapter<String> adapterTipoUni = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, carregaTipoUni());
        dropdownTipoUni.setAdapter(adapterTipoUni);

        //Caso for alterar pega o ID por parametro
        Bundle parametros = getIntent().getExtras();

        if(parametros != null){
            idListaInt = parametros.getInt("id");
            idItem = parametros.getInt("idItem");
            TextView desctLista = (TextView)findViewById(R.id.textViewDescLista);
            Lista pegaTitulo = null;
            try {
                pegaTitulo = getListaDao().queryForId(idListaInt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            desctLista.setText("Lista "+pegaTitulo.getId()+" - "+pegaTitulo.getNome());
            if(idItem > 0){
                try {
                    item = getItemDao().queryForId(idItem);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                dropdownDepartamentos.setSelection(idDepartamentos.indexOf(item.getDepartamento_id()));
                dropdownProdutos.setSelection(idProdutos.indexOf(item.getProduto_id()));
                dropdownTipoUni.setSelection(idTiposUni.indexOf(item.getTipouni_id()));
                mEditQtd.setText(String.valueOf(item.getQtd()));

                alteracao = true;
            }else{
                Button bExcluir = (Button)findViewById(R.id.buttonExcluir);
                bExcluir.setVisibility(View.INVISIBLE);
            }

        }
    }

    public ArrayList<String> carregaDeps() {
        ArrayList<String> dados = new ArrayList<String>();
        ArrayList<Long> ids = new ArrayList<Long>();

        try {
            deparmentoLista  = getDepartamentoDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        int id;
        for(int i = 0; i < deparmentoLista .size(); i++){
            dados.add(deparmentoLista.get(i).getId() +" - " + deparmentoLista.get(i).getNome().toString());
            ids.add(deparmentoLista.get(i).getId());
        }
        idDepartamentos = ids;
        return dados;

    }

    public ArrayList<String> carregaProdutos() {
        ArrayList<String> dados = new ArrayList<String>();
        ArrayList<Long> ids = new ArrayList<Long>();

        try {
            produtoLista  = getProdutoDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        int id;
        for(int i = 0; i < produtoLista .size(); i++){
            dados.add(produtoLista.get(i).getId() +" - " + produtoLista.get(i).getNome().toString());
            ids.add(produtoLista.get(i).getId());
        }
        idProdutos = ids;
        return dados;
    }

    public ArrayList<String> carregaTipoUni() {
        ArrayList<String> dados = new ArrayList<String>();
        ArrayList<Long> ids = new ArrayList<Long>();

        try {
            tipoUniLista  = getTipoUniDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        int id;
        for(int i = 0; i < tipoUniLista .size(); i++){
            dados.add(tipoUniLista.get(i).getId() +" - " + tipoUniLista.get(i).getNome().toString());
            ids.add(tipoUniLista.get(i).getId());
        }
        idTiposUni = ids;
        return dados;
    }

    public boolean validar(){
        EditText mQTD;

        Spinner dropdownDepartamentos = (Spinner)findViewById(R.id.dropdownDepartamento);
        Spinner dropdownProdutos = (Spinner)findViewById(R.id.dropdownProduto);
        Spinner dropdownTipoUni = (Spinner)findViewById(R.id.dropdownTipoUni);

        mQTD = (EditText)findViewById(R.id.editTextQtd);

        if (mQTD.getText().toString().length() < 1
                || dropdownDepartamentos.getSelectedItemPosition() == AdapterView.INVALID_POSITION
                || dropdownProdutos.getSelectedItemPosition() == AdapterView.INVALID_POSITION
                || dropdownTipoUni.getSelectedItemPosition() == AdapterView.INVALID_POSITION ){

            Toast.makeText(this, "Informe os campos corretamente!", Toast.LENGTH_LONG).show();
            return false;
        }
        if(Double.parseDouble(mQTD.getText().toString()) <= 0){
            Toast.makeText(this, "A quantidade deve ser maior que 0!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void salvar(View view){
        EditText mQTD;

        Spinner dropdownDepartamentos = (Spinner)findViewById(R.id.dropdownDepartamento);
        Spinner dropdownProdutos = (Spinner)findViewById(R.id.dropdownProduto);
        Spinner dropdownTipoUni = (Spinner)findViewById(R.id.dropdownTipoUni);

        mQTD = (EditText)findViewById(R.id.editTextQtd);

        if (validar()){
            Double qtd = Double.parseDouble(mQTD.getText().toString());
            if(alteracao){
                try {
                    Item itemAlterado = null;

                    try {
                        itemAlterado = getItemDao().queryForId(idItem);
                    } catch (SQLException e) {
                        Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    //Seta os valores da tela
                    itemAlterado.setQtd(qtd);
                    itemAlterado.setDepartamento_id(idDepartamentos.get(dropdownDepartamentos.getSelectedItemPosition()));
                    itemAlterado.setTipouni_id(idTiposUni.get(dropdownTipoUni.getSelectedItemPosition()));
                    itemAlterado.setProduto_id(idProdutos.get(dropdownProdutos.getSelectedItemPosition()));

                    if (getItemDao().update(itemAlterado) < 1){
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    }else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }else{
                item = new Item(
                        qtd,
                        idDepartamentos.get(dropdownDepartamentos.getSelectedItemPosition()),
                        idTiposUni.get(dropdownTipoUni.getSelectedItemPosition()),
                        idProdutos.get(dropdownProdutos.getSelectedItemPosition()),
                        Long.parseLong(String.valueOf(idListaInt).toString())
                );

                try {
                    if(getItemDao().create(item) < 1)
                        Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
                    else{
                        cancelar(view);
                    }
                } catch (SQLException e) {
                    Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    public void cancelar(View view){
        /*Intent intent = intent = new Intent(this, ItensActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", idListaInt);
        startActivity(intent);*/
        Intent intent = new Intent(produtosListaActivity.this, ItensActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", idListaInt);
        startActivity(intent);

    }

    public void excluir(View view){
        try {
            Item deletar = getItemDao().queryForId(idItem);
            if(getItemDao().delete(deletar) < 1){
                Toast.makeText(view.getContext(), "Falha", Toast.LENGTH_LONG).show();
            }else{
                cancelar(view);
            }
        } catch (SQLException e) {
            Toast.makeText(view.getContext(), "Dados não foram gravados! Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
