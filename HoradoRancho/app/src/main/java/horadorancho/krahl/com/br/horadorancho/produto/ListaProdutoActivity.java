package horadorancho.krahl.com.br.horadorancho.produto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;
import horadorancho.krahl.com.br.horadorancho.tag.Tag;
import horadorancho.krahl.com.br.horadorancho.tag.TagActivity;

public class ListaProdutoActivity extends Activity implements ListView.OnItemClickListener{

    private List<Produto> produtoLista = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<Produto, Integer> getTagDao() throws SQLException {
        Dao<Produto, Integer> dao = DaoManager.createDao(getConnectionSource(), Produto.class);
        return dao;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_produto);

        ListView lista = (ListView) findViewById(R.id.listaProduto);
        ArrayList<String> produtos = preencheDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produtos);
        lista.setAdapter(arrayAdapter);

        lista.setOnItemClickListener(this);
    }

    private ArrayList<String> preencheDados() {
        ArrayList<String> dados = new ArrayList<String>();

        try {
            produtoLista = getTagDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        for(int i = 0; i < produtoLista.size(); i++){
            dados.add(produtoLista.get(i).getNome().toString());
        }

        return dados;

    }

    public void adicionarProduto(View view) {
        Intent intent = new Intent(ListaProdutoActivity.this, ProdutoActivity.class);
        startActivity(intent);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String teste = parent.getItemAtPosition(position);
        Intent intent = new Intent(ListaProdutoActivity.this, ProdutoActivity.class);

        intent.putExtra("id",produtoLista.get(position).getId().intValue());
        startActivity(intent);
    }
}
