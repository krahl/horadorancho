package horadorancho.krahl.com.br.horadorancho.tipouni;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import horadorancho.krahl.com.br.horadorancho.OpenDatabaseHelper;
import horadorancho.krahl.com.br.horadorancho.R;

public class ListaTipoUniActivity extends Activity implements ListView.OnItemClickListener {

    private List<TipoUni> tipoLista = null;

    private ConnectionSource getConnectionSource(){
        return new OpenDatabaseHelper(this).getConnectionSource();
    }

    private Dao<TipoUni, Integer> getTipoUniDao() throws SQLException {
        Dao<TipoUni, Integer> dao = DaoManager.createDao(getConnectionSource(), TipoUni.class);
        return dao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tipo_uni);

        ListView lista = (ListView) findViewById(R.id.listaTipoUni);
        ArrayList<String> tipos = preencheDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tipos);
        lista.setAdapter(arrayAdapter);

        lista.setOnItemClickListener(this);
    }

    private ArrayList<String> preencheDados() {
        ArrayList<String> dados = new ArrayList<String>();

        try {
            tipoLista = getTipoUniDao().queryForAll();
        } catch (SQLException e) {
            Toast.makeText(this, "Detalhes:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        for(int i = 0; i < tipoLista.size(); i++){
            dados.add(tipoLista.get(i).getNome().toString()+" - "+tipoLista.get(i).getSigla());
        }

        return dados;

    }

    public void adicionarTipoUni(View view) {
        Intent intent = new Intent(ListaTipoUniActivity.this, TipoUniActivity.class);
        startActivity(intent);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String teste = parent.getItemAtPosition(position);
        Intent intent = new Intent(ListaTipoUniActivity.this, TipoUniActivity.class);

        intent.putExtra("id",tipoLista.get(position).getId().intValue());
        startActivity(intent);
    }
}
